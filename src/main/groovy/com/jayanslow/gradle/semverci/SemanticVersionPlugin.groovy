package com.jayanslow.gradle.semverci

import de.skuzzle.semantic.Version
import org.gradle.api.InvalidUserDataException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.slf4j.LoggerFactory

import java.util.function.Predicate
import java.util.regex.Pattern

/**
 * Plugin which configures the project version.
 */
class SemanticVersionPlugin implements Plugin<Project> {
  static Predicate<String> normalize(object, Pattern defaultPattern) {
    List<Object> objects
    if (object == null) {
      objects = [defaultPattern]
    }
    else if (object instanceof Iterable) {
      objects = object.asList()
    }
    else {
      objects = [object]
    }
    List<Predicate<String>> predicates = objects.collect {predicate ->
      if (predicate instanceof String) {
        return {x -> x == (String) predicate} as Predicate<String>
      }
      else if (predicate instanceof Pattern) {
        return {x -> x.matches(predicate)} as Predicate<String>
      }
      else if (predicate instanceof Predicate<String>) {
        return predicate
      }
      throw new IllegalArgumentException("Expected config to be a String or Pattern (or a List of those elements); was ${it.getClass()}")
    }
    return {x -> predicates.any {it.test(x)}} as Predicate<String>
  }

  static VersionConfig parse(SemanticVersionPluginExtension ext) {
    return [
            development     : normalize(ext.development, ~/^develop$/),
            releaseCandidate: normalize(ext.releaseCandidate, ~/^(?:release|hotfix)\/v?(\d+\.\d+\.\d+)$/),
            release         : normalize(ext.release, ~/^v?(\d+\.\d+\.\d+)$/)
    ] as VersionConfig
  }

  private logger = LoggerFactory.getLogger('semver-ci')

  @Override
  void apply(final Project project) {
    def extension = project.extensions.create('semver', SemanticVersionPluginExtension)

    def config = parse(extension)
    def f = new BuildVersionFunction(config)

    String rawBaseVersion = project.property('version')

    if (rawBaseVersion == null) {
      throw new InvalidUserDataException("Base version not set. The `version` gradle property must be set.")
    }

    Version baseVersion
    try {
      baseVersion = Version.parseVersion(rawBaseVersion)
    }
    catch (Version.VersionFormatException e) {
      throw new InvalidUserDataException("Base version is not a valid semantic version (http://semver.org/); was '$rawBaseVersion'.")
    }

    logger.debug "Base version of '${project.name}' is '$baseVersion'"

    logger.debug "Calculating new version (from '${baseVersion}')"
    def newVersion = f.apply(baseVersion)

    logger.info "Setting project '${project.name}' (and sub-projects) version to '$newVersion'"
    project.version = newVersion.toString()

    project.subprojects {
      it.version = newVersion.toString()
      logger.debug "Set sub-project '${it.name}' version"
    }

    logger.debug 'Finished'
  }

}
